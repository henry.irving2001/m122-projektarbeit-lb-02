# Projekt Dokumentation

[[_TOC_]]

## Lösungsdesign
Anhand der Analyse wurde folgendes Lösungsdesign entworfen.

### Aufruf der Skripte

#### Script 1
Das Script wird dann aufgerufen, wenn der Lehrer den Fortschritt / die Commits der Lernende sehen will.

Beim Abrufen des Scripts müssen folgende Parameter mitgegeben werden:
    - Input File mit sämtlichen GIT-URLs und Zielverzeichnisnamen
    - Lokales Base-Verzeichnis als Kommandline-Parameter
    - (optional) Als Kommandline-Parameter Debug Option Aktivieren

<pre> git_clone_update_repos.bash repolist.txt /tmp/myrepos </pre>

#### Script 2
Beim Abrufen des Scripts muss der Output-Filename als Kommandline-Parameter mitgegeben werden. Auch muss ein Verzeichniss als Parameter mitgegeben werden. Mit dem optionalem -debug Parameter können Konsol Outputs vom Debug Level gesehen werden. 

<pre> git_extract_commits.bash /tmp/myrepos /tmp/commits.csv -debug</pre>

### Ablauf der Automation

#### Script 1
![activity_diagram_lb02_script1.png](./activity_diagram_lb02_script1.png)
#### Script 2
![image.png](./image.png)

### Konfigurationsdateien

#### Script 1 & 2
Die Konfigdatei wird zwischen Script 1 und 2 geteilt. Momentan gibt es keine daten welche in dieser Konfigdatei gespeichert werden muss, jedoch könnten diese später im entwicklungsprocess auftauchen.

## Abgrenzungen zum Lösungsdesign

TODO: Nachdem das Programm verwirklicht wurde hier die unterschiede von der Implemenatino zum Lösungsdesign beschreiben (was wurde anders gemacht, was wurde nicht gemacht, was wurde zusaetzlich gemacht)
