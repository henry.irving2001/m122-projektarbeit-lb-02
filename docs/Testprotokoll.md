Skript 1

| Testfall | Testbeschreibung | Testdaten | erwartetes Testresultat | erhaltenes Testresultat | Tester | Testdatum und Teststatus |
|  - | - | - | - | - | - | - |
| Erstmaliger Aufruf | Das Skript soll mit einem input file aufgerufen werden, in welchem nur verfügbare Git-URLs sind. Diese sollen in ein noch nicht existierendes Verzeichnis geklont werden:<pre>skript1.bash repolist.txt /tmp/myrepos</pre> | repolist.txt mit folgendem Inhalt:<pre>https://gitlab.com/armindoerzbachtbz/m122_praxisarbeit<br>https://gitlab.com/wapdc/InfoSearch/Project-2017</pre> | Verzeichnis wird erstellt und alle Repos werden darin geklont | Script kann aufgerufen werden | Justin Raúl Feliz | 09.05.2022 |

| Testfall 2 | Testbeschreibung | Testdaten | erwartetes Testresultat | erhaltenes Testresultat | Tester | Testdatum und Teststatus |
|  - | - | - | - | - | - | - |
| Fehlermeldung bei falschen Parametern | Wenn falsche Parameter eingegeben werden, soll dies nach der Überprüfung einen Fehlermeldung ausgeben. | repolist.txt mit falschem Inhalt wie z.B.: <pre> Benjamin_Harlacher_Michael_de_Smitt https://gitlab.com/MDS160902/m122_praxisarbeit_desmitt_harlacher   | Ausgabe von einer Fehlermeldung: <pre> Wrong input. Please check the formatting <br> of your input file.</pre> | | | |


| Testfall 3| Testbeschreibung | Testdaten | erwartetes Testresultat | erhaltenes Testresultat | Tester | Testdatum und Teststatus |
|  - | - | - | - | - | - | - |
| Andere Fehlerhafte Ausführung des Scripts | Sollte das Script nicht durchlaufen aufgrund von einem Fehler, wie eine ungültige URL, keine Internetverbindung, keine Permission, etc. soll das Script den Fehler aus dem git log ausgeben. | repolist.txt mit folgendem Inhalt:<pre>https://gitlab.com:MDS160902/m122_praxisarbeit_desmitt_harlacher Benjamin_Harlacher_Michael_de_Smitt</pre> <br> Ausführung vom Script ohne Internetverbindung | Ausgabe von einer Fehlermeldung, welche aus dem Log stammt, z.B.: <pre> Connection failed --> No network connection</pre> | Nicht implementiert --> Fehlerhaft danach Zeitmangel | Justin Raúl Feliz | 09.05.2022 |


| Testfall 4| Testbeschreibung | Testdaten | erwartetes Testresultat | erhaltenes Testresultat | Tester | Testdatum und Teststatus |
|  - | - | - | - | - | - | - |
| Git Pull wenn das Verzeichnis bereits existiert | Falls das zu klonende Verzeichnis bereits im Zielverzeichnis existiert soll nur ein git pull ausgeführt werden anstelle eines git clone. | repolist.txt mit folgendem Inhalt, wie z.B.:<pre>https://gitlab.com/MDS160902/m122_praxisarbeit_desmitt_harlacher<br>https://gitlab.com/Kilian-Epp/m122_luca_kilian  <br> https://gitlab.com/henry.irving2001/m122-projektarbeit-lb-02</pre> | Bei den Verzeichnissen, die bereits existieren wird ein git pull ausgeführt. | Git Pull wird ausgeführt| Justin Ra~ul Feliz | 09.05.2022 |









Skript 2

| Testfall 01 | Testbeschreibung | Testdaten | erwartetes Testresultat | erhaltenes Testresultat | Tester | Testdatum und Teststatus |
|  - | - | - | - | - | - | - |
| Erstmaliger Aufruf | Das Skript soll mit einem Verzeichnis als parameter augerufen werden in welchem 2 Repos sind:<pre> git_extract_commits.bash /tmp/myrepos /tmp/commits.csv</pre> | Verzeichnis mit den GIT-Repos die mit dem Skript 1 geklont wurden:<pre>/tmp/myrepos</pre> | Alle Repos aus /tmp/myrepos werden gelesen und ein File /tmp/commits.csv erstellt mit allen Commits beider Repos | File | justin | 09.05.2022 |


| Testfall 02 | Testbeschreibung | Testdaten | erwartetes Testresultat | erhaltenes Testresultat | Tester | Testdatum und Teststatus |
|  - | - | - | - | - | - | - |
| Erstmaliger Aufruf | Das Skript wird aufgerufen während im Base Repo sich eine Repo befindet welches es nicht mehr gibt, oder auf welches man keinen Zugriff hat:<pre> git_extract_commits.bash /tmp/myrepos /tmp/commits.csv</pre> | Verzeichnis mit den GIT-Repos die mit dem Skript 1 geklont wurden:<pre>/tmp/myrepos</pre> | Der git log failed. Die Error Message wird in einem Logfile geloggt und dem User wird eine passenede Error Message ausgegeben. | fehlermeldung klappt noch nicht ganz | justin |  09.05.2022 |

| Testfall 03 | Testbeschreibung | Testdaten | erwartetes Testresultat | erhaltenes Testresultat | Tester | Testdatum und Teststatus |
|  - | - | - | - | - | - | - |
| Erstmaliger Aufruf | Das Script wird mit einem Falschem / nicht existierendem Verzeichnis als Parameter ausgeführt. <pre> git_extract_commits.bash /tmp/thisRepoDoesntExist /tmp/commits.csv</pre>| Verzeichnis mit den GIT-Repos die mit dem Skript 1 geklont wurden:<pre>/tmp/myrepos</pre> | Da das Verzeichnis nicht existiert gibt es einen Error, welcher im Log File geloggt wird, und mit einer Passenden Error Message dem User angezeigt wird. | fehlermeldung | justin | 09.05.2022 |

| Testfall 04 | Testbeschreibung | Testdaten | erwartetes Testresultat | erhaltenes Testresultat | Tester | Testdatum und Teststatus |
|  - | - | - | - | - | - | - |
| Erstmaliger Aufruf | Das Skript soll mit einem Verzeichnis als parameter augerufen werden in welchem 1 Repos ist. Zusätzlich wird der Optionaler Debug Parameter beigefügt.<pre> git_extract_commits.bash /tmp/myrepos /tmp/commits.csv -debug</pre> | Verzeichnis mit den GIT-Repos die mit dem Skript 1 geklont wurden:<pre>/tmp/myrepos</pre> | Alle Repos aus /tmp/myrepos werden gelesen und ein File /tmp/commits.csv erstellt mit allen Commits beider Repos. Zusätzlich werden jetzt nicht nur Info und Error Messages in der Console angezeigt, sondern auch Warnings und jegliche andere Consol Ausgaben. | wie erwartet | justin | 09.05.2022 |

| Testfall 05 | Testbeschreibung | Testdaten | erwartetes Testresultat | erhaltenes Testresultat | Tester | Testdatum und Teststatus |
|  - | - | - | - | - | - | - |
| Erstmaliger Aufruf | Das Skript soll mit einem Verzeichnis als parameter augerufen welches existiert aber leer ist. <pre> git_extract_commits.bash /tmp/myrepos /tmp/commits.csv</pre> | Verzeichnis mit den GIT-Repos die mit dem Skript 1 geklont wurden:<pre>/tmp/myrepos</pre> | Da das Repo leer ist, wird dem User eine Info Message angezeigt, welche auch geloggt wird. | wie erwartet | justin | 09.05.2022 |
