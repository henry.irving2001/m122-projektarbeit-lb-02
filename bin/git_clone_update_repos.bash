#!/bin/bash

# Version, with  
VERSION = 0.1.0

#basic information

cwd= $(pwd)									# current work directory
cd $(dirname $0)							# cd to where the script is
bindir = $ (pwd)							# bindir is the directory where the script is located
cd $cwd 									# go back to current work directory
basename = $ (basename $0)					# set script name 
configdir = $bindir/../etc					# the config file directory
tempdir = $bindir/../etc					# Temporary directory




#check if user gave a valid directory
if [ -z $1 ]; 
	then
	
		echo E "Invalid base directory"
	
	exit 1
fi 

#create temporary directory
mkdir -p $tempdir

#Remove all files, that are not listed within the file 
for dir in $( ls $1 );
do 
# check if repo is in the file
	if ! grep -q $dir $tempdir/repos 
		then  
			echo "Removing $dir"
			rm -rf $1/dir
	fi

done

#get repos from config file
$configdir/git_clone_update_repos.bash.env | grep -v '^#' | grep -v '`$' | while read urlrepo author; 
do
	# get repo name from URL 
	reponame = $ (echo $urlrepo | awk -F/ '{print $NF}')	
	
	# Check if the repo directory exists
	if [ ! -d "$1/reponame" ];
		then 
	# put name into temp file
			echo ""
			git clone $urlrepo $1/$reponame
	
		echo $reponame >>$tempdir/repos
	
	# git pull if repo already exists
	else
		echo $reponame >>$tempdir/repos
		cd $1/$reponame
		echo ""
		git pull
		
	fi
	

done

