#!/bin/bash
set -o errexit                  # abort on nonzero exitstatus
cwd=$(pwd)                      # current working directory
cd "$(dirname "$0")"            # change to the directory where the script is located
BINDIR=$(pwd)                   # BINDIR: the directory where the script is located
cd "$cwd"                       # return to the working directory
BASENAME=$(basename "$0")       # Set the script name (without path to it)
TMPDIR=/tmp/$BASENAME.$$        # Set a temporary directory if needed
ETCDIR=$BINDIR/../etc           # ETCDIR is the config directory
directoryName=$1
outputFileName=$2

log(){
    echo "$1"
}

function checkParameters() {
  if [ -z "$directoryName" ]
  then
      log "No directory param" 3
  fi
  if [ -z "$outputFileName" ]
  then
      log "No file path param" 3
  fi
}

function printTitle(){
  fileString="Zielverzeichnis, Datum, Commit-Hash, Author\n"

  GitRepositories=($(find $directoryName -name .git))
  for gitRepo in "${GitRepositories[@]}"
  do
      GitRepo=$(sed 's/.git//g' <<< "$gitRepo")
      cd $GitRepo
      BasenameGitFolder=$(basename $GitRepo);
      log "extracting log from Folder: $BasenameGitFolder" 1
      fileString+=$(git log --pretty=format:"$BasenameGitFolder,%cd,%H,%an" --date=format:'%Y%m%d')
      fileString+="\n"
  done
}

function logRepos(){
  checkParameters
  printTitle
  log "File created $outputFileName" 1
  echo -e "$fileString" >> $outputFileName
}

logRepos
